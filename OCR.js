function countVerticalIslands(matrix){
    let middle = matrix[0].length/2|0;
    let count = 0, i = 0, L = matrix.length;
    while(i < L)
        if(matrix[i][middle] == 1){
            count++;
            while(i < L && matrix[i][middle] == 1)
                i += 1;
        }else
            i += 1;
    return count;
}


function countInternalZeros(matrix){
    const S = JSON.stringify, H = matrix.length, W = matrix[0].length;
    let count = 0;
    let visited = new Set(), y, x, flag, stack,i, j;
    for(i  = 0; i < H; i++){
        for(j = 0; j < W; j++){
            if(matrix[i][j] == 0 && !visited.has(S([i, j]))){
                stack = [[i, j]];
                flag = 0;
                while(stack.length){
                    [y, x] = stack.pop();
                    for(let [y_, x_] of [[y, x + 1], [y, x- 1], [y + 1, x], [y - 1, x]]){
                        if(matrix[y_] == undefined || matrix[y_][x_] == undefined){
                            flag = 1;
                        }
                        else if(matrix[y_][x_] == 0 && !visited.has(S([y_, x_]))){
                            stack.push([y_, x_]);
                        }
                    }
                    visited.add(S([y, x]));
                }

                if(flag != 1)
                    count++;
            }
        }
    }
    return count
}

function countHorizontalIslands(matrix, index){
    index = (index * matrix.length) | 0;
    let count = 0, i = 0, L = matrix[0].length;
    while(i < L)
        if(matrix[index][i] == 1){
            count++;
            while(i < L && matrix[index][i] == 1)
                i += 1;
        }else
            i += 1;
  
;
    return count;
}

function ocr(image)
{
    let matrix = [], tempRow = [];
    let _ = Infinity;
    let __ = -Infinity, i, j;
    for(i = 0; i < image.pixels.length; i+= image.width){
        tempRow = [];
        for(j = i; j < i + image.width; j++){
            tempRow.push(image.pixels[j]);
            _ = _ > image.pixels[j] ? image.pixels[j] : _
            __ = __ > image.pixels[j] ? __ : image.pixels[j];
        }
        matrix.push(tempRow);
    }
    let THRESOLD = 1 + (_ + __)/2 | 0;
    let digits = [];
    let last = Array(matrix.length).fill(1).map(x=>[]), flag = 0, temp = [], las = false, pe;
    
    for(i = 0; i < matrix[0].length; i++){
        flag = 0, temp = [];
        for(j = 0; j < matrix.length; j++){
            pe = 1-(matrix[j][i]/THRESOLD|0);
            temp.push(pe);
            if(pe==1)
                flag = 1;
        }

        if(flag == 1){
            for(let k = 0; k < matrix.length; k++){
                last[k].push(temp[k]);
            }
            las = true;
        }
        else if(flag == 0 && las == true){
            digits.push(last);
            last = Array(matrix.length).fill(1).map(x=>[]);
            las = false;
        }
    }

    if(last[0].length){
        digits.push(last);
    }




    let result = '', f1, verti, zeros, exp, l1;
    for(let _ of digits)
    {
        i = _.slice(_.findIndex(x=>x.includes(1)), _.length - _.findIndex((x,u)=>_[_.length - 1 - u].includes(1)));
        if(i.length<2)
            continue
        verti = countVerticalIslands(i);
        if(verti == 3){
                exp = i.length * 0.30 | 0;
                l1 = i[exp].lastIndexOf(1)/i[exp].length;
                if(l1 < 0.5)
                    result += '5';
                else{
                    exp = i.length * 0.67 | 0; 
                    if(i[exp].lastIndexOf(1)/i[exp].length < 0.5)
                        result += '2';
                    else if((i[i.length * 0.33 | 0].indexOf(1)/i[i.length * 0.33 | 0].length > 0.5))
                        result += '3';
                    else
                        result += "8";
                  }
        }
        else if(verti == 2){
            if(countInternalZeros(i) == 0)
                result += '7';
            else{
                if(i[i.length * 0.67 | 0].indexOf(1)/i[0].length > 0.5)
                    result += '9';
                else if(i[i.length * 0.33 | 0].lastIndexOf(1)/i[0].length < 0.5)
                    result += '6';
                else
                    result += '0';
            }
        }
        else if(verti == 1){
            if(countHorizontalIslands(i, 0.75) == 2)
                result += '1';
            else
                result += '4';
        }
    }

    return result;
}
